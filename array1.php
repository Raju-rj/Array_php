<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


</head>
<body>
<?php
$city = array("Dhaka","Chittagong","Rajshahi","Sylet","Khulna","Barishal");
print_r($city);
echo "<br>";
$array1=[
    "Apple",
    "Banana",
    true,
    5.5,5
];

print_r($array1);
echo "<br>";

echo "<pre>";
$array1=[
    "Apple",
    "Banana",
    true,
    5.5,5
];

var_dump($array1);
echo "</pre>";

$array = array("Nighat","Parvin","Hasan","asde");
echo print_r($array);
echo "<br>";
$array2=array(5=>"a",2=>"b",4=>"c",3=>"d");

print_r($array2);
$array2=[
    "a"=>"Apple",
    "b"=>"Banana",
    "c"=>"Cherry",
    "d"=>"Dragon Fruit"
];

print_r($array2);
echo "<br>";
$array1=[
    "Apple",
    "Banana",
    "c"=>"Cherry",
    "d"=>"Dragon Fruit"
];

print_r($array1);
echo "<br>";
$array1=[
    "Apple",
    "Banana",
    "c"=>"Cherry",
    "d"=>"Dragon Fruit"
];

foreach($array1 as $val){
    echo $val,"<br>";
}
echo "<br>";
$array1=[
    "Apple",
    "Banana",
    "c"=>"Cherry",
    "d"=>"Dragon Fruit"
];

foreach($array1 as $key=>$val){
    echo $key,"<br>";
}
echo "<br>";
$array1=[
    "Apple",
    "Banana",
    "c"=>"Cherry",
    "d"=>"Dragon Fruit"
];

foreach($array1 as $key=>$val){
    echo "$key=$val","<br>";
}

$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
$age = array("Nighat"=>"35", "Ben"=>"37", "Joe"=>"43");
$age = array("Shamim"=>"35", "Ben"=>"37", "Joe"=>"43");

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}

?>

////////////////////////////////////////////////////////////////



<?php

//PHP 5 Array Functions
echo "<br>";
echo "<h3>values of an indexed array:</h3>";
echo "<br>";
$city = array("Dhaka","Chittagong","Khulna","Shylet");
$arrlength=count($city);

for($i=0;$i<$arrlength;$i++){

    echo $city[$i]."<br>";


}

// A two-dimensional array
echo "<br>";
echo "<h3>A two-dimensional array</h3>";
echo "<br>";
$cars =array
   (
     array("Volvo",100,96),
     array("BMW",60,90),
     array("Toyota",101,99)

   );
echo $cars[0][0].":Ordered: ".$cars[0][1].".Sold: ".$cars[0][2]."<br>";
echo $cars[1][0]."Ordered: ".$cars[1][1]."Sold: ".$cars[1][2]."<br>";
echo $cars[2][0]."Ordered: ".$cars[2][1]."Sold: ".$cars[2][2]."<br>";




//Multidimensional Arrays
echo "<br>";
echo "<h3>Multidimensional Arrays</h3>";
echo "<br>";

$cars = array
(
    array("Volvo",22,18),
    array("BMW",15,13),
    array("Saab",5,2),
    array("Land Rover",17,15)
);

for ($row = 0; $row < 4; $row++) {
    echo "<p><b>Row number $row</b></p>";
    echo "<ul>";
    for ($col = 0; $col < 3; $col++) {
        echo "<li>".$cars[$row][$col]."</li>";
    }
    echo "</ul>";
}
//array_change_key_case()

echo "<br>";
echo "<h3>array_change_key_case()</h3>";
echo "<br>";


$nighat = array("a"=>"Cat","b"=>"Dog","c"=>"Horse","d"=>"Brid");
print_r(array_change_key_case($nighat,CASE_UPPER));
echo "<br>";
echo "<br>";

$shamiha =array("Cat"=>"40","Dog"=>"70","Horse"=>"13","Brid"=>"67","Camal"=>"90");
print_r(array_change_key_case($shamiha,CASE_UPPER));
echo "<br>";
echo "<br>";

$shamiha =array("A"=>"Cat","B"=>"Dog","C"=>"Horse","D"=>"Cat","Brid"=>"Cat","F"=>"Camal",);
print_r(array_change_key_case($shamiha,CASE_LOWER));
echo "<br>";
echo "<br>";

$shamiha =array("Cat"=>"40","Dog"=>"70","Horse"=>"13","Brid"=>"67","Camal"=>"90");
print_r(array_change_key_case($shamiha,CASE_LOWER));
echo "<br>";
echo "<br>";


//array_chunk()

echo "<br>";
echo "<h3>array_chunk()</h3>";
echo "<br>";


echo "<pre>";
$fruit=array("Mango","Banana","Lici","Guava","Pinapple","Griup");
print_r(array_chunk($fruit,2));
echo "<br>";
print_r(array_chunk($fruit,3));
echo "</pre>";


//array_column()
echo "<br>";
echo "<h3>array_column()</h3>";
echo "<br>";
$a = array(
    array(
        'id' => 5698,
        'first_name' => 'Peter',
        'last_name' => 'Griffin',
    ),
    array(
        'id' => 4767,
        'first_name' => 'Ben',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 3809,
        'first_name' => 'Joe',
        'last_name' => 'Doe',
    )
);

$first_names= array_column($a,'first_name');
print_r($first_names);
echo "<br>";
echo "<br>";

$last_names = array_column($a, 'last_name');
print_r($last_names);


//array_combine() Function
echo "<br>";
echo "<h3>array_combine() Function</h3>";
echo "<br>";
$fname = array("Nighat","Parvin","Shamim","Shamiha","Shabiha");
$age = array("28","18","32","5","3");
$c=array_combine($fname,$age);
print_r($c);

//array_count_values()
echo "<br>";
echo "<h3>array_count_values()</h3>";
print_r(array_count_values($fname));
echo "<br>";

//array_diff()
echo "<br>";
echo "<h3>array_diff()</h3>";
$b=array("a"=>"red","b"=>"green","c"=>"pink","d"=>"blue");
$a=array("e"=>"red","f"=>"green","g"=>"pink","h"=>"blue","i"=>"lightblue","j"=>"yellow");
$color=array_diff($a,$b);
print_r($color);
echo "<br>";


//array_diff_assoc()
echo "<br>";
echo "<h3>array_diff_assoc()</h3>";
$a=array("a"=>"red","b"=>"green","c"=>"pink","d"=>"blue");
$b=array("a"=>"red","b"=>"green","g"=>"pink","d"=>"blue","i"=>"lightblue","j"=>"yellow");
$color=array_diff_assoc($a,$b);
print_r($color);
echo "<br>";

$color=array_diff_key($a,$b);
print_r($color);
echo "<br>";

//array_flip()
echo "<br>";
echo "<h3>array_flip()</h3>";
$a=array("a"=>"red","b"=>"pink","c"=>"green","d"=>"blue");
$color=array_flip($a);
print_r($color);

//array_intersect()
echo "<br>";
echo "<h3>array_intersect() </h3>";
$a=array("a"=>"red","b"=>"pink","c"=>"green","d"=>"blue");
$b=array("a"=>"red","b"=>"green","g"=>"pink","d"=>"parpul");
$color=array_intersect($a,$b);
print_r($color);

//array_key_exists()
echo "<br>";
echo "<h3>array_key_exists() </h3>";
$a=array("a"=>"red","b"=>"pink","c"=>"green","d"=>"blue");

if(array_key_exists("b",$a)){
    echo "Key Is Exists!";
}
else{
    echo "Key does not exist";
}
//array_map()
echo "<br>";
echo "<h3>array_map() </h3>";
 function myname($variable)
 {
     return($variable+$variable);
 }
 $a=array(1,2,3,4,5,6);
 print_r(array_map("myname",$a));


//array_merge_recursive()
echo "<br>";
echo "<h3>array_merge_recursive()</h3>";
echo "<pre>";
$a1=array("a"=>"red","b"=>"green","c"=>"pink","d"=>"lightblue");
$a2=array("c"=>"blue","b"=>"yellow");
print_r(array_merge_recursive($a1,$a2));
echo "</pre>";

// array_multisort()
echo "<br>";
echo "<h3> array_multisort() </h3>";
$a1=array("red","green","pink","lightblue");
array_multisort($a1);
print_r($a1);

?>

</body>
</html>